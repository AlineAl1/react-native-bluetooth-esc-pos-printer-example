import React from 'react';

import {Theme, TamaguiProvider, Button} from 'tamagui';

import config from './tamagui.config';

export default function App() {
  return (
    <TamaguiProvider config={config}>
      <Theme name="dark">
        <Button>Hello world</Button>
      </Theme>
    </TamaguiProvider>
  );
}
