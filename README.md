## Usage de ce package:

 - [GitHub - januslo/react-native-bluetooth-escpos-printer](https://github.com/januslo/react-native-bluetooth-escpos-printer)

## Autres documents consultés

  - [GitHub - prawito/bluetooth-printer-react-native](https://github.com/prawito/bluetooth-printer-react-native)
  - ["Créer une application Bluetooth pour ESP32 avec React Native" par AranaCorp.](https://www.aranacorp.com/fr/creer-une-application-bluetooth-pour-esp32-react-native/amp/)
  - ["Communicating with BLE devices with Expo mobile apps" par Theodo.](https://blog.theodo.com/2023/07/ble-integration-for-rn-apps/)

## Les étapes à suivre pour installer le package:

```
npm install react-native-bluetooth-escpos-printer --save
```
- Aller dans _android/settings.gradle_ puis ajouter 
```
include ':react-native-bluetooth-escpos-printer'
project(':react-native-bluetooth-escpos-printer').projectDir = new File(rootProject.projectDir, '../node_modules/react-native-bluetooth-escpos-printer/android')’
```
- Aller dans _android/app/build.gradle_ puis ajouter 

```
dependencies {
  ...
  implementation project(':react-native-bluetooth-escpos-printer')
}
```
- Ajouter dans _MainApplication.java_
```
import cn.jystudio.bluetooth.RNBluetoothEscposPrinterPackage
```
- Aller dans _node_modules/react-native-bluetooth-escpos-printer/android/src/main/java/cn/jystudio/bluetooth/RNBluetoothManagerModule_ puis remplacer les 
```
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
```
par 
```
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
```
- Aller dans _node_modules/react-native-bluetooth-escpos-printer/android/build.gradle_ et remplacer tous les:
```
 jcenter { url "http://jcenter.bintray.com/" }, maven {url "http://repo.spring.io/plugins-release/"} des repositories. 
```
par 
```
jcenter { url "https://jcenter.bintray.com/" },  maven {url "https://repo.spring.io/plugins-release/"}
```
- Changer les versions SDK pour la faire correspondre à celle utilisée dans l’application (ceci est un exemple):

```
compileSdkVersion 33
buildToolsVersion "33.0.0"

defaultConfig {
    minSdkVersion 21
    targetSdkVersion 33
    versionCode 1
    versionName "1.0"
}
```
## Exemple de création d’un Provider pour permettre la connexion automatique de l’imprimante à partir des appareils déjà paired sur le téléphone:  

- Gestion des permissions, il faut en effet la permission utilisateur pour scanner les appareils bluetooth et les localiser
